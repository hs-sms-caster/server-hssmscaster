/**
 * Created by Sibtain Raza on 1/9/2017.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.set('view engine', 'ejs')


// var USER = "Users";
// var GROUPS = "Groups";
// var USERCONTACTS = "UserContacts";
// var USERSENTMESSAGES = "UserSentMessages";
// exports.TABLE_USER = USER;
// exports.USER = USER;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// API CLASSES - PATH DEFINE TO MAKE IS USABLE

var user                            =   require("./api/usersApiResponses");
var group                           =   require("./api/groupsApiResponses");
var userContactsApiResponses        =   require("./api/userContactsApiResponses");
var userSentMessagesApiResponses    =   require("./api/userSentMessagesApiResponses")

// API CLASSES - INCLUDING IN APP

app.use("/user",user);
app.use("/group",group);
app.use("/userContact",userContactsApiResponses);
app.use("/userSentMessage",userSentMessagesApiResponses);

// app.use('/groupimages',     express.static('./uploadedImages/groupImages'));



app.listen(3000, function () {
    console.log('HS SMS Caster - Node Server Started - Working On Port 3000');
});
